const express = require("express");
const path = require("path");
const app = express();
const multer = require("multer"); //modul buat upload foto
const axios = require("axios"); //modul buat get di dalem router
const moment = require("moment"); //modul buat parsing, nampilin tanggal dan waktu
const bodyParser = require("body-parser");
const fs = require("fs");
const { Car, Size } = require("./models");
// const { Size } = require("./models");
const controllers = require("./controllers");

//fungsi buat setting tujuan folder dan nama file buat foto yg diupload
const diskStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "./uploads"));
  },
  // konfigurasi penamaan file yang unik
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

// Ambil port dari environment variable
// Dengan nilai default 8022
const PORT = process.env.PORT || 8022;
const PUBLIC_DIRECTORY = path.join(__dirname, "./public");

// Set format request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(PUBLIC_DIRECTORY));
app.use(express.static("uploads"));
app.set("view engine", "ejs");
app.use(bodyParser.json());

// Menampilkan index
app.get("/", controllers.api.car.list);

// GET /cars/create dari file create.ejs
app.get("/cars/formCreate", controllers.api.car.formCreate);

//Create a new car
app.post(
  "/cars/create",
  multer({ storage: diskStorage }).single("photo"),
  controllers.api.car.create
);

//get all cars
app.get("/cars", controllers.api.car.list);

//==========================================//
//DARI SINI BELUM DIBUAT DESIGN PATTERN NYA//
//GET /cars/edit-car/:id dari file edit-car.ejs
app.get("/edit-car/:id", (req, res) => {
  axios
    .get("http://localhost:8020/cars/" + req.params.id)
    .then((response) => {
      res.render("cars/edit-car", { car: response.data, moment: moment });
    })
    .catch((err) => {
      res.status(404).send("Car not found!");
    });
});

// GET /cars/:id/update
app.get("/cars/:id", async (req, res) => {
  // const car = getCar(req.params.id);
  try {
    const car = await Car.findOne({
      where: { id: req.params.id },
    });
    res.status(200).json(car);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.post(
  "/edit-car/:id",
  multer({ storage: diskStorage }).single("photo"),
  async (req, res) => {
    // const car = getCar(req.params.id);
    try {
      const { id_size, name, price, size } = req.body;
      const photo = req.file.filename;
      const cars = await Car.findOne({
        where: { id: req.params.id },
      });
      if (cars) {
        if (req.file) {
          fs.unlink(path.join(__dirname, "./uploads/" + cars.photo), (err) => {
            if (err) {
              console.log(err);
            }
          });
        }
        await Car.update(
          {
            id_size: id_size,
            name: name,
            price: price,
            size: size,
            photo: photo,
          },
          {
            where: { id: req.params.id },
          }
        );
        res.send(
          '<script>window.location.href="/";document.getElementById("alert-update").click();</script>'
        );
      } else {
        res.send(
          '<script>window.location.href="/";document.getElementById("alert-not-found").click();</script>'
        );
        // res.render("index", { cars });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
);

app.get("/cars/:id/delete", (req, res) => {
  Car.destroy({
    where: { id: req.params.id },
  })
    .then((car) => {
      res.send(
        '<script>window.location.href="/";document.getElementById("alert-save").click();</script>'
      );
    })
    .catch((err) => {
      res.status(422).send("Can't delete car");
    });
});

app.listen(PORT, () => {
  console.log(`Server nyala di http://localhost:${PORT}`);
});
