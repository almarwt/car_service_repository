const { Car } = require("../models");

module.exports = {
  findAll() {
    return Car.findAll();
  },

  create(createBody) {
    return Car.create(createBody);
  },


};