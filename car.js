const { Car } = require("./models");
let cars = [];

function createCar(car) {
  // car.id = cars.sort((a, b) => b.id - a.id)[0]?.id || 1;
  car.id = cars.length + 1;
  cars.push(car);
  return car;
}

function updateCar(id, ucar) {
  let car;

  cars = cars.map((i) => {
    if (i.id !== Number(id)) return i;
    car = {
      ...i,
      ...ucar,
    };
    return car;
  });

  return car;
}

function listCars() {
  return cars;
}

function deleteCar(id) {
  cars = cars.filter((i) => i.id !== Number(id));
}

function getCar(id) {
  return cars.find((i) => i.id === Number(id));
}

module.exports = {
  listCars,
  createCar,
  updateCar,
  deleteCar,
  getCar,
};
