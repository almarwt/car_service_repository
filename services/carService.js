const carRepository = require("../repositories/carRepository");

module.exports = {
  async list() {
    try {
      const cars = await carRepository.findAll();
      return cars;
    } catch (err) {
      throw err;
    }
  },

  create(requestBody) {
    return carRepository.create(requestBody);
  },


};
