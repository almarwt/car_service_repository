const carService = require("../../services/carService");
const moment = require("moment");

module.exports = {
    list(req, res) {
      carService
        .list()
        .then((cars) => {
          console.log(cars)
          res.render("index", { cars, moment: moment });
        });
    },

    formCreate(req, res) {
        res.render("cars/create");
    },

    create(req, res) {
      carService
        .create({
          id_size: req.body.id_size,
          name: req.body.name,
          price: req.body.price,
          size: req.body.size,
          photo: req.file.filename,
        })
        .then((car) => {
          console.log(car);
          res.send(
            '<script>window.location.href="/";document.getElementById("alert-save").click();</script>'
          );
        })
        .catch((err) => {
          res.status(422).json({
            status: "FAIL",
            message: err.message,
          });
        });
    },
    
};